#ifndef NUAGE_HPP
#define NUAGE_HPP
#include <string>
#include <iostream>
#include <vector>
#include <string.h>
#include <stack>
#include "Point.hpp"

using namespace std;

class Nuage
{
    private :
    unsigned int nb_points;
    vector<Point> vect_points;
    stack<Point> Conv;
    void printStack(const stack<Point> pile) const; //Pour le debug des différentes piles
    int GenererFichierGnuplot(const string chemin1,const string chemin2) const; //Pour générer les fichiers gnuplot temporaires par l'appel de GNUPlot()
    stack<Point> ES(); //Retourne l'enveloppe convexe supérieure
    stack<Point> EI(); //Retourne l'enveloppe convexe inférieure
    void EConv(); //Met a jour l'attribut Conv
    int Orientation(const Point A,const Point B,const Point C); //1 en cas de positif, -1 en cas de négatif

    public:
    void print() const;
    Nuage();
    Nuage(const unsigned int nb_points);
    Nuage(Nuage const &Nuage_a_copier);
    Nuage(const string chemin);
    int AjouterPoint(const float x,const float y); // 0 en cas de réussite 1 en cas d'échec
    int SupprimerPoint(const float x,const float y); // 0 en cas de réussite 1 en cas d'échec
    int GenererFichier(const string chemin) const; //0 en cas de réussite 1 en cas d'échec
    int GNUPlot() const;
    unsigned int getNb_Points() const;

    

};
#endif 