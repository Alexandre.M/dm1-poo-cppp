CFLAGS= -W -Wall -Iinclude/# -ansi
LDFLAGS= -Iinclude/
CC=g++
SRC= $(wildcard src/*.cpp)
OBJ = ${subst src,obj,$(SRC:.cpp=.o)}
RM = rm -f

default: main

main : main.cpp $(OBJ)
	$(CC) $^ -o $@ $(LDFLAGS)

obj/%.o : src/%.cpp
	$(CC) -o $@ -c $< $(CFLAGS)

test : test/test.cpp $(OBJ)
	$(CC) $^ -o ./main_test $(LDFLAGS)


.PHONY: clean

clean:
	$(RM) obj/*.o main data/* main_test

