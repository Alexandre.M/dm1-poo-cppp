#include "Nuage.hpp"
#include <cstdlib> // Accès a srand et rand
#include <time.h> // Accès a time pour l'init de srand
#include <fstream> //Accès aux fichiers
#include <algorithm> //sort
#include <iostream> //system
using namespace std;


Nuage::Nuage()
{
    /* 
    Input : void
    Constructeur par défault
    */
    nb_points=0;
}

Nuage::Nuage(const unsigned int nb_points)
{
    /* 
    Input : const unsigned int nb_points
    Constructeur créant un nuage de nb_points aléatoires
    */
    Nuage::nb_points=nb_points;
    srand(time(NULL));
    for (unsigned int i=0;i<nb_points;i++)
    {
        Point p;
        p.x =(float)(rand()%1000000)/1000000;//random
        p.y =(float)(rand()%1000000)/1000000;//random
        vect_points.push_back(p);
    }
    //On trie les points par ordre croissant
    sort(vect_points.begin(),vect_points.end(),[](Point a,Point b){
        return (a.x<b.x)||(a.x==b.x && a.y<b.y);
    });
    //On calcule l'enveloppe Convexe
    EConv();
}

Nuage::Nuage(const string chemin)
{
    /* 
    Input : const string chemin
    Constructeur créant un nuage a partir d'un fichier au format supposé correct
    */

    //Cette fonction "charge" un nuage de point
    ifstream fichier;
    fichier.open(chemin);
    if (!fichier.is_open())
    {
        cerr << "Erreur d'ouverture/de création du fichier";
        exit(1);
    }
    fichier >> nb_points;
    for (unsigned int i = 0; i < nb_points; i++)
    {
        Point p;
        fichier >> p.x;
        fichier >> p.y;
        vect_points.push_back(p);
    }
}

Nuage::Nuage(Nuage const &Nuage_a_copier)
{
    /* 
    Input : Nuage const &Nuage_a_copier
    Constructeur par copie de la classe Nuage
    */
    nb_points=Nuage_a_copier.nb_points;
    for (unsigned int i = 0; i < Nuage_a_copier.nb_points; i++)
    {
        vect_points.push_back(Nuage_a_copier.vect_points[i]);
    }
    //Pas besoin de trier car normalement déjà fait
    //On calcule l'enveloppe Convexe
    EConv();
}

int Nuage::AjouterPoint(const float x,const float y)
{
    /* 
    Input : const float x , y
    Ajoute un point au Nuage de point
    Output :    0 si pas d'erreur
    */
    nb_points++;
    Point p;
    p.x=x;
    p.y=y;
    vect_points.push_back(p);
    //On trie à nouveau
    sort(vect_points.begin(),vect_points.end(),[](Point a,Point b){
        return (a.x<b.x)||(a.x==b.x && a.y<b.y);
    });
    //On calcule l'enveloppe Convexe
    EConv();
    return 0;
}

int Nuage::SupprimerPoint(const float x,const float y)
{
    /* 
    Input : const float x,y
    Méthode qui essaye de supprimer un point dans le Nuage de point s'il existe
    Output :    0 si point trouvé et supprimé
                1 si point non trouvé et donc non supprimé
    */
    //On ne supprimera qu'une fois le point, s'il existe en plusieurs exemplaire c'est le problème de l'utilisateur
    int pt_supprime =0;
    for(unsigned int i=0;i<nb_points;i++)
    {
        if (vect_points[i].x == x && vect_points[i].y == y)
        {
            vect_points.erase(vect_points.begin()+i);
            pt_supprime=1;
            break;
        }
    }
    if (pt_supprime==0)
    {
        cout << "Erreur, ce point n'existe pas x="<< x << " y=" << y << endl;
        return 1;
    }
    nb_points--;
    //Le tableau des points est déjà trié 
    //On calcule l'enveloppe Convexe
    EConv();
    return 0;
}

void Nuage::print() const
{
    /* 
    Input : void
    Méthode imprimant le nuage de points
    Output : void
    */
    cout << "Nombre de points : " << nb_points << endl;
    for (unsigned int i=0;i<nb_points;i++)
    {
        cout << "Point n°"<< i << " :  x : " << vect_points[i].x << "   y : " << vect_points[i].y << endl;
    }
    cout << endl << "Enveloppe Convexe : " << endl;
    printStack(Conv);
}


void Nuage::printStack(const stack<Point> pile) const
{
    /* 
    Input : const stack<Point> pile
    Méthode privée qui imprime une pile de point, pour le debug
    Output : void
    */
    cout << "Taille de la pile : " << pile.size() << " points" << endl;
    stack<Point> Piletemp;
    Piletemp = pile; //Copy constructor de stack 
    for (unsigned int i=0;i<pile.size();i++)
    {
        cout << "Point n°"<< i << " :  x : " << Piletemp.top().x << "   y : " << Piletemp.top().y << endl;
        Piletemp.pop();
    }
    //cout << pile.size() << "(normalement inchangé)" <<endl;
}

int Nuage::GenererFichier(const string chemin) const
{
    /* 
    Input : const string chemin
    Crée un fichier au format suivant :
        nb_point
        point1.x    point1.y
        point2.x    point2.y    
    Output :    0 si fichier créé
                1 si erreur d'ouverture
    */
    ofstream fichier;
    fichier.open(chemin);
    if (!fichier.is_open())
    {
        cout << "Erreur d'ouverture/de création du fichier";
        return 1;
    }
    fichier << nb_points << "\n";
    for (unsigned int i = 0; i < nb_points; i++)
    {
        fichier << vect_points[i].x << " " << vect_points[i].y << "\n";
    }
    fichier.close();
    return 0;
}

int Nuage::GenererFichierGnuplot(const string chemin1,const string chemin2) const
{
    /* 
    Input : const string chemin1,chemin2
    Méthode privée qui crée deux fichiers pour GNUPLOT au format suivant à partir de l'attribut Conv et vect_points
    point1.x    point1.y
    point2.x    point2.y



    Output :    0 si fichiers créées
                1 si erreur de lecture
    */
    ofstream fichier1;
    ofstream fichier2;
    fichier1.open(chemin1);
    if (!fichier1.is_open())
    {
        cout << "Erreur d'ouverture/de création du fichier";
        return 1;
    }
    fichier2.open(chemin2);
    if (!fichier2.is_open())
    {
        cout << "Erreur d'ouverture/de création du fichier";
        return 1;
    }

    for (unsigned int i = 0; i < nb_points; i++)
    {
        fichier1 << vect_points[i].x << " " << vect_points[i].y << "\n";
    }
    fichier1.close();

    //Dans le fichier 2 on dépile Conv
    stack<Point> Convtemp ;
    Convtemp = Conv; //Passage par copie de stack
    long unsigned int taille = Convtemp.size();
    for (long unsigned int i = 0; i < taille; i++)
    {
        fichier2 << Convtemp.top().x << " " << Convtemp.top().y << "\n";
        Convtemp.pop();
    }
    fichier2.close();

    return 0;
}

int Nuage::GNUPlot() const
{
    /* 
    Input : void
    On crée les deux .png du nuage avec et sans l'enveloppe convexe, en utilisant GNUPLOT
    Output :    0 si pas d'erreur
                1 si erreur d'ouverture
    */
    string chemin1 = "./data/gnuplot_data_nuage.txt";
    string chemin2 = "./data/gnuplot_data_conv.txt";
    int retour = GenererFichierGnuplot(chemin1,chemin2);
    if (retour==1){return 1;}
    const char * cmdscriptConv = "gnuplot ./scriptConv.p";
    const char * cmdscript = "gnuplot ./scriptSansConv.p";
    //On a généré les fichiers, maintenant on utilise systeme pour générer gnuplot
    system(cmdscriptConv);
    system(cmdscript);
    //Je supprime les fichiers générés par GenererFichierGnuplot
    const char *rmfiles = "rm ./data/gnuplot_data_nuage.txt ./data/gnuplot_data_conv.txt";
    system(rmfiles);
    return 0;
}

int Nuage::Orientation(const Point A,const Point B,const Point C)
{
    /* 
    Input : Point A , B , C
    On cherche l'orientation en calculant de det(AB,AC)
    Output :    1 si det positif
                -1 si det négatif
    */
    float det;
    det = (B.x-A.x)*(C.y-A.y)-(B.y-A.y)*(C.x-A.x);
    if (det>0)
    {
        return 1;
    }
    return -1;
}

stack<Point> Nuage::ES()
{
    /* 
    Input : void
    On crée l'enveloppe supérieure du Nuage de point
    Output :    stack<Point>
    */
    //La base de la pile est le premier Point de vect et le second
    stack<Point> PileES;
    PileES.push(vect_points[0]);
    PileES.push(vect_points[1]);

    cout << "Pile ES premier : " << endl;
    printStack(PileES);
    //Maintenant algorithme :
    //On va balayer tout le nuage de points
    cout << endl << endl << "==========Création de l'enveloppe Supérieure==========" << endl << endl;
    for (unsigned int i = 2; i < nb_points; i++)
    {
        Point ElmM1,ElmM2;
        ElmM1=PileES.top();
        PileES.pop();
        ElmM2=PileES.top();

        //Debug 
        cout << "Element : (" << vect_points[i].x << "," << vect_points[i].y << ")" << endl;
        cout << "Element-1 : (" << ElmM1.x << "," << ElmM1.y << ")" << endl;
        cout << "Element-2 : (" << ElmM2.x << "," << ElmM2.y << ")" << endl;


        if (Orientation(vect_points[i],ElmM1,ElmM2)>0)
        {
            //On a bien une orientation positive, on remet l'élément -1 et on rajoute le nouvel élément sur le haut de la pile
            PileES.push(ElmM1);
            PileES.push(vect_points[i]);
            cout << "Orientation positive, élément rajouté" << endl << endl;
        }
        else
        {
            //On ne remet pas élément -1 dans la pile
            //Si la pile n'a plus qu'un élément on rajoute le nouvel élément
            cout << "Orientation négative, élément rejeté" << endl;
            if (PileES.size()==1)
            {
                PileES.push(vect_points[i]);
                cout << "On ajoute car il ne reste plus qu'un élément" << endl << endl;
            }
            else
            {
                //On va continuer d'essayer d'ajouter vect_points[i] actuel avec les nouveaux ElmM1 et ElmM2, pour continuer a
                //travailler avec vect_points[i] on effectue i = i-1
                i=i-1;
                cout << "On supprime ElmM1" << endl << endl;
            }
        }
        
    }
    printStack(PileES);
    return PileES;  
}

stack<Point> Nuage::EI()
{
    /* 
    Input : void
    On crée l'enveloppe inférieure du Nuage de point
    Output :    stack<Point>
    */
    //La base de la pile est le premier Point de vect et le second
    stack<Point> PileEI;
    PileEI.push(vect_points[0]);
    PileEI.push(vect_points[1]);

    cout << endl << endl << "==========Création de l'enveloppe Inférieure==========" << endl << endl;

    //Maintenant algorithme :
    //On va balayer tout le nuage de points
    for (unsigned int i = 2; i < nb_points; i++)
    {
        Point ElmM1,ElmM2;
        ElmM1=PileEI.top();
        PileEI.pop();
        ElmM2=PileEI.top();

        //Debug 
        cout << "Element : (" << vect_points[i].x << "," << vect_points[i].y << ")" << endl;
        cout << "Element-1 : (" << ElmM1.x << "," << ElmM1.y << ")" << endl;
        cout << "Element-2 : (" << ElmM2.x << "," << ElmM2.y << ")" << endl;

        if (Orientation(vect_points[i],ElmM1,ElmM2)<0)
        {
            //On a bien une orientation négative, on remet l'élément -1 et on rajoute le nouvel élément sur le haut de la pile
            PileEI.push(ElmM1);
            PileEI.push(vect_points[i]);

            cout << "Orientation négative, élément rajouté" << endl << endl;
        }
        else
        {
            cout << "Orientation positive, élément rejeté" << endl;
            //On ne remet pas élément -1 dans la pile
            //Si la pile n'a plus qu'un élément on rajoute le nouvel élément
            if (PileEI.size()==1)
            {
                PileEI.push(vect_points[i]);
                cout << "On ajoute car il ne reste plus qu'un élément" << endl << endl;
            }
            else
            {
                //On va continuer d'essayer d'ajouter vect_points[i] actuel avec les nouveaux ElmM1 et ElmM2, pour continuer a
                //travailler avec vect_points[i] on effectue i = i-1
                i=i-1;
                cout << "On supprime ElmM1" << endl << endl;
            }
        }
        
    }
    printStack(PileEI);
    return PileEI;  
}

void Nuage::EConv()
{
    /* 
    Input : void
    On créé l'attribut Conv de l'objet
    Output :    void
    */
    //On vide l'ancien attribut Conv
    long unsigned int taille = Conv.size();
    for (long unsigned int i = 0; i < taille; i++)
    {
        Conv.pop();
    }
    
    stack<Point>PileES = ES();
    stack<Point>PileEI = EI();

    cout << endl << "DEBUG On imprime conv DEBUG" <<endl << "Attendu : 0 points" << endl;
    printStack(Conv);

    cout << endl << endl << "==========Création de Conv==========" << endl << endl;

    // ES est lue de la gauche vers la droite, je vais l'inverser
    stack<Point>PileESInversee;
    taille = PileES.size();
    for (long unsigned int i = 0; i < taille; i++)
    {
        PileESInversee.push(PileES.top());
        PileES.pop();
    }
    //On met toute la PileESInversée dans Conv
    taille = PileESInversee.size();
    for (long unsigned int i = 0; i < taille; i++)
    {
        Conv.push(PileESInversee.top());
        PileESInversee.pop();
    }
    //On se débarasse du top de PileEI et on la met dans Conv
    PileEI.pop();
    taille = PileEI.size();
    for (long unsigned int i = 0; i < taille; i++)
    {
        Conv.push(PileEI.top());
        PileEI.pop();
    }
    //On a l'enveloppe convexe du nuage
    cout << endl << "Enveloppe convexe :" <<endl;
    printStack(Conv);
}

unsigned int Nuage::getNb_Points() const
{
    /* 
    Input : void
    On récupère l'attribut privé nb_points
    Output : unsigned int
    */
    return nb_points;
}