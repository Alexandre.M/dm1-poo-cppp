#define CATCH_CONFIG_MAIN
#include "Nuage.hpp"
#include "catch.hpp"

//Afin de respecter le principe d'encapsulation, on ne traite pas les méthodes privées

SCENARIO("Les nuages sont modifiables","[Nuage]")
{
    GIVEN("Un nuage de point connu"){
        Nuage MonNuage = Nuage("test/load_nuage.txt");

        WHEN("Un point est rajouté"){
            int valretour = MonNuage.AjouterPoint(0.5,0.5);

            THEN("Le nombre de points est augmenté")
            {
                REQUIRE(MonNuage.getNb_Points()==6);
                REQUIRE(valretour==0);
            }
        }
        WHEN("Un point est enlevé")
        {
            std::cout << "Ici on a nb points = " << MonNuage.getNb_Points() << endl;
            int valretour = MonNuage.SupprimerPoint(0.069954,0.043483);

            THEN("Le nombre de points est diminué")
            {
                REQUIRE(MonNuage.getNb_Points()==4);
                REQUIRE(valretour==0);
            }
        }
        WHEN("On essaie de supprimer un point qui n'existe pas")
        {
            std::cout << "Ici on a nb points = " << MonNuage.getNb_Points() << endl;
            int valretour = MonNuage.SupprimerPoint(0.024954,0.243483);
            THEN("On a un retour de type erreur (1), et les nb de points restent inchangés")
            {
                REQUIRE(MonNuage.getNb_Points()==5);
                REQUIRE(valretour==1);
            }
        }
    }
}

